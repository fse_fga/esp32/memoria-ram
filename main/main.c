#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_heap_caps.h"
#include "esp_log.h"

void Task1(void * param)
{
  int stack_task = uxTaskGetStackHighWaterMark(NULL);
  ESP_LOGI("TASK", "Tamanho livre na Stack %d", (int) stack_task);

  char array1 [8000];
  memset(array1, 1, sizeof(array1));

  stack_task = uxTaskGetStackHighWaterMark(NULL);
  ESP_LOGI("TASK", "Tamanho livre na Stack %d", (int) stack_task);

  while(true)
  {
    vTaskDelay(1000 / portTICK_PERIOD_MS);
    char *array;
    array = (char *) malloc(15000 * sizeof(char));
    memset(array, 1, 15000);
    ESP_LOGI("Task 1", "Espaço livre na HEAP %dkb", (int) xPortGetFreeHeapSize()); 
    free(array);
  }
}

void app_main()
{
  int quantidade = 3;
  float valor = 15.0;

  printf("Quantidade: %d | Valor: %f \n", quantidade, valor);

  // Alocação de memória HEAP
  ESP_LOGI("RAM", "Espaço livre na HEAP - Pré-alocação    %d b", (int) xPortGetFreeHeapSize());

  char *array;
  array = (char *) malloc(2048 * sizeof(char));
  if(array == NULL){
    ESP_LOGE("RAM", "Falha na alocação de memória");
  }
  else
  {
    memset(array, 1, 2048);
    for(int i = 0; i < 2048; i++)
    {
      quantidade += array[i];
    }
  }
  printf("Quantidade %d\n", quantidade);
  ESP_LOGI("RAM", "Espaço livre na HEAP - Pós-alocação    %d b", (int) xPortGetFreeHeapSize()); 
  free(array);
  ESP_LOGI("RAM", "Espaço livre na HEAP - Pós-desalocação    %d b", (int) xPortGetFreeHeapSize()); 

  int DRAM = heap_caps_get_free_size(MALLOC_CAP_8BIT);
  int IRAM = heap_caps_get_free_size(MALLOC_CAP_32BIT) - heap_caps_get_free_size(MALLOC_CAP_8BIT);

  ESP_LOGI("RAM", "Data RAM:        %d b", DRAM);
  ESP_LOGI("RAM", "Instruction RAM: %d b", IRAM);  

  // Stack
  int stack_main = uxTaskGetStackHighWaterMark(NULL);
  ESP_LOGI("RAM", "Tamanho livre na Stack %d", (int) stack_main);

  xTaskCreate(&Task1, "Tarefa", 10000, NULL, 1, NULL);

  vTaskDelay(1000 / portTICK_PERIOD_MS);
  ESP_LOGI("RAM", "Espaço livre na HEAP - Após iniciar TASK %dkb", (int) xPortGetFreeHeapSize()); 

}